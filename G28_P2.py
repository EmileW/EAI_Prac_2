"""
EAI 320 Practical 2 : Genetic Algorithm

Group Members: Emile Wepener u21494780
               Muller Pretorius u21444553
               
Date : 31/03/2024

Notes for Self:                 

"""
import numpy as np
import csv
import random
import matplotlib.pyplot as plt

# Variables for Plot
population_fitness_gen = list()
max_fitness_gen = list()
average_fitness_gen = list()

# Constants
POPULATION_SIZE = 20
MUTATION_RATE = 0.05 # In % chance of mutatindg a gene
NUM_GENERATIONS = 20
SURVIVAL_RATE = 0.6
CROSSOVER_RATE = 0.3
Training_Data = 0 # 0 = Data1.csv and 1 = Data2 csv


# All the possible histories.
dictionary = ["RRRR", "RRRP","RRRS", "RRPR","RRPP", "RRPS","RRSR", "RRSP",
              "RRSS", "RPRR","RPRP", "RPRS","RPPR", "RPPP","RPPS", "RPSR",
              "RPSP", "RPSS","RSRR", "RSRP","RSRS", "RSPR","RSPP", "RSPS",
              "RSSR", "RSSP","RSSS", "PRRR","PRRP", "PRRS","PRPR", "PRPP",
              "PRPS", "PRSR","PRSP", "PRSS","PPRR", "PPRP","PPRS", "PPPR",
              "PPPP", "PPPS","PPSR", "PPSP","PPSS", "PSRR","PSRP", "PSRS",
              "PSPR", "PSPP","PSPS", "PSSR","PSSP", "PSSS","SRRR", "SRRP",
              "SRRS", "SRPR","SRPP", "SRPS","SRSR", "SRSP","SRSS", "SPRR",
              "SPRP", "SPRS","SPPR", "SPPP","SPPS", "SPSR","SPSP", "SPSS",
              "SSRR", "SSRP","SSRS", "SSPR","SSPP", "SSPS","SSSR", "SSSP",
              "SSSS"]

def read_csv(file_path):
    data = []
    with open(file_path, 'r', newline='') as csvfile:
        csvreader = csv.reader(csvfile)
        data = list(csvreader) # The responses to each history, loaded from the generated CSV file.
    return data

# Read the history data from the csv files
file_path1 = 'data1.csv'
file_path2 = 'data2.csv'
data1 = read_csv(file_path1)
data2 = read_csv(file_path2)

def create_random_individuals(size):
    gene_list = list()
    for i in range(0,size):
        gene_list.append(np.random.choice(['R', 'P', 'S']))
        
    return gene_list

def create_population(population_size):
    population = list()
    for i in range(0,population_size):
        population.append(create_random_individuals(81))
    return population

def winning_move(move):
    if move == "R":
        return "P"
    elif move == "S":
        return "R"
    else:
        return "S"
    
def create_fittness_matrix(population):
    # To calulate the fittness of the population
    # a one is given to gene if the gene plays a winning move and zero if it does not.
    # The total for the 81 genes is then given to the individual
    # This is repeated for all of the individuals of the population.
    # Thus the maximum fitness of an indivisual is 1000000
    
    if Training_Data == 0:
        data = data1
    else:
        data = data2
    # data_81_rows = data#data[csv_row_num:csv_row_num+81]
    fittness = list()
    
    for individual in population:
        indiv_fittness_sum = 0        
        for row in data:
            opponent_move = row[1]
            history = row[0]
            hist_pos_dict = dictionary.index(history)
            if (individual[hist_pos_dict] == winning_move(opponent_move)):
                indiv_fittness_sum +=1
        fittness.append(indiv_fittness_sum)
    return fittness

def add_mutations(individual):    
    for i in range(0,len(individual)):
        if random.random() < MUTATION_RATE:
            if (individual[i] == "R"):
                individual[i] = np.random.choice(['P', 'S'])
            elif (individual[i] == "P"):
                individual[i] = np.random.choice(['R', 'S'])
            else:
                individual[i] = np.random.choice(['R', 'P'])
    
    return individual    

def create_children(parent1, parent2):
    num_genes = int(81/CROSSOVER_RATE)
    genes11 = parent1[:num_genes]
    genes12 = parent1[num_genes:]
    genes21 = parent2[:num_genes]
    genes22 = parent2[num_genes:]
    
    child1 = genes11 + genes22
    child2 = genes21 + genes12
    
    # Add mutations
    child1 = add_mutations(child1)
    child2 = add_mutations(child2)
    
    return [child1,child2]
    
# Start of algorithm
if Training_Data == 0:
    data = data1
else:
    data = data2
    

population = create_population(POPULATION_SIZE)
for gen in range(0,NUM_GENERATIONS):
    print("Generation:" +" "+ str(gen+1))
    # Get the fitness of this generation 
    fittness_mat = create_fittness_matrix(population)
    print("Individual Fitnesses: ")
    print(np.multiply(fittness_mat,1/len(data))*100)
    average_fitness = 0
    for fit in fittness_mat:
        average_fitness+=fit
    average_fitness = average_fitness / POPULATION_SIZE
    print("Average Fitness: ")
    print(str(average_fitness/len(data)*100) + "%")
    
    # Save the fitness for the graphs.
    population_fitness_gen.append(fittness_mat)
    # max_fitness_gen.append((max(fittness_mat)/len(data))*100)
    max_fitness_gen.append(max(fittness_mat))
    # average_fitness_gen.append(average_fitness/len(data)*100)
    average_fitness_gen.append(int(average_fitness))

    
    
    # Now sort the population from highest to lowest fitness
    for i in range(0,POPULATION_SIZE):
        population[i] = [population[i],fittness_mat[i]]
    population = sorted(population, key=lambda x: x[1], reverse=True)
    # Remove fitness
    for i in range(0,POPULATION_SIZE):
        population[i] = population[i][0]
        
    # Now remove ("kill") the worst preforming part of the population
    new_pop_size = int(POPULATION_SIZE*SURVIVAL_RATE)
    population = population[:new_pop_size]
    
    # Now we need more childen in the population
    i = 0
    while(len(population) < POPULATION_SIZE):
        # Get children
        children = create_children(population[i],population[i+1])
        i+=2
        # Add Child 1 to population
        population.append(children[0])
        # Add Child 2 to population
        population.append(children[1])

# Save the data
with open('population_fitness.csv', 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerows(population_fitness_gen)
    
with open('max_fitness.csv', 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(max_fitness_gen)
    
with open('average_fitness.csv', 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(average_fitness_gen)
'''       
# for i in range(0,NUM_GENERATIONS):     
#     # Plot the data
#     x = list(range(1, POPULATION_SIZE+1))
#     y = np.multiply(np.divide(population_fitness_gen[i], len(data)), 100)
#     bars = plt.bar(x, y, label="Fitness", color="red")

#     # Annotate each bar with its value
#     # for bar, val in zip(bars, y):
#     #     plt.text(bar.get_x() + bar.get_width() / 2, val, f'{val:.2f}%', 
#     #              ha='center', va='bottom', fontsize=8)

#     # x-axis label
#     plt.xlabel('Individual in Population')
#     # y-axis label
#     plt.ylabel('Fitness (%)')
#     # plot title
#     plt.title('Fitness of population, generation ' + str(i+1))

#     # Set x-axis ticks to show only full integers
#     plt.xticks(range(1, POPULATION_SIZE+1))
#     # Set y-axis limits from 0 to 100
#     plt.ylim(20, 80)
    
#     plt.figtext(0.5, 0.01, 'Plot of the individual fitnesses. Population Size = '
#                 +str(POPULATION_SIZE)+' Mutation Rate = '+ str(MUTATION_RATE)+
#                 ' Survival Rate = '+str(SURVIVAL_RATE) + ' Crossover Rate = ' + str(CROSSOVER_RATE)
#                 , ha='center', fontsize=10, wrap=True)

#     # showing legend
#     plt.legend()
#     # Save plot
#     plt.savefig('population_fitness_plot_'+str(i+1)+'_'+str(POPULATION_SIZE)+'_'+str(NUM_GENERATIONS)+'.png', bbox_inches='tight')
#     # function to show the plot
#     plt.show()

# # Average fitness plot
# # Calculate y-values
# y = np.multiply(np.divide(average_fitness_gen, len(data)), 100)
# x = list(range(1, NUM_GENERATIONS+1))
# plt.figure(figsize=(16, 6))
# # Plot the line
# plt.plot(x, y, label='Average Fitness', color='blue',linestyle='--')

# # Overlay dots at each data point
# plt.scatter(x, y, color='red')

# # Annotate each data point with its value
# # for i, (xi, yi) in enumerate(zip(x, y)):
# #     plt.annotate(f'{yi:.2f}%', (xi, yi), textcoords="offset points", xytext=(0, 10), ha='center')

# # x-axis label
# plt.xlabel('Generation')
# # y-axis label
# plt.ylabel('Average Fitness of population (%)')
# # plot title
# plt.title('Average fitness of the population vs Genetic Algorithm Generation')
# # Set x-axis ticks to show only full integers
# plt.xticks(range(1, NUM_GENERATIONS+1))
# # Set y-axis limits from 0 to 100
# plt.ylim(20, 100)

# # Add caption
# plt.figtext(0.5, 0.01, 'Plot of the average population fitness. Population Size = '
#             +str(POPULATION_SIZE)+' Mutation Rate = '+ str(MUTATION_RATE)+
#             ' Survival Rate = '+str(SURVIVAL_RATE) + ' Crossover Rate = ' + str(CROSSOVER_RATE)
#             , ha='center', fontsize=10, wrap=True)

# # showing legend
# plt.legend()
# # Your plotting code here...

# # Save the plot with the adjusted figure size
# plt.savefig('average_fitness_plot_'+'_'+str(POPULATION_SIZE)+'_'+str(NUM_GENERATIONS)+'.png', bbox_inches='tight', dpi=300)
# # function to show the plot
# plt.show()


# # Maximum fitness
# # Average fitness plot
# # Calculate y-values
# y = np.multiply(np.divide(max_fitness_gen, len(data)), 100)
# x = list(range(1, NUM_GENERATIONS+1))

# plt.figure(figsize=(16, 6))
# # Plot the line
# plt.plot(x, y, label='Maximum Fitness', color='blue',linestyle='--')

# # Overlay dots at each data point
# plt.scatter(x, y, color='red')

# # Annotate each data point with its value
# # for i, (xi, yi) in enumerate(zip(x, y)):
# #     plt.annotate(f'{yi:.2f}%', (xi, yi), textcoords="offset points", xytext=(0, 10), ha='center')

# # x-axis label
# plt.xlabel('Generation')
# # y-axis label
# plt.ylabel('Maximum Fitness of population (%)')
# # plot title
# plt.title('Maximum fitness of the population per genetartion of GA')
# # Set x-axis ticks to show only full integers
# plt.xticks(range(1, NUM_GENERATIONS+1))
# # Set y-axis limits from 0 to 100
# plt.ylim(20, 100)

# # Add caption
# plt.figtext(0.5, 0.01, 'Plot of the maximum population fitness. Population Size = '
#             +str(POPULATION_SIZE)+' Mutation Rate = '+ str(MUTATION_RATE)+
#             ' Survival Rate = '+str(SURVIVAL_RATE) + ' Crossover Rate = ' + str(CROSSOVER_RATE)
#             , ha='center', fontsize=10, wrap=True)

# # showing legend
# plt.legend()

# plt.savefig('max_fitness_plot_'+'_'+str(POPULATION_SIZE)+'_'+str(NUM_GENERATIONS)+'.png', bbox_inches='tight')
# # function to show the plot
# plt.show()
'''

# Calculate y-values for average fitness plot
y_average = np.multiply(np.divide(average_fitness_gen, len(data)), 100)
x_average = list(range(1, NUM_GENERATIONS+1))

# Calculate y-values for maximum fitness plot
y_maximum = np.multiply(np.divide(max_fitness_gen, len(data)), 100)

# Get the maximum fitness value
max_fitness = max(y_maximum)
max_average = max(y_average)
# Create a new figure
plt.figure(figsize=(18, 6))

# Plot the average fitness line
plt.plot(x_average, y_average, label='Average Population Fitness', color='blue', linestyle='--')

# Overlay dots at each data point for average fitness
plt.scatter(x_average, y_average, color='blue')

# Plot the maximum fitness line on the same plot
plt.plot(x_average, y_maximum, label='Best Individual Fitness', color='red', linestyle='--')

# Overlay dots at each data point for maximum fitness
plt.scatter(x_average, y_maximum, color='red')

# Set x-axis label
plt.xlabel('Generation')
# Set y-axis label
plt.ylabel('Fitness (%)')
# Set plot title
plt.title('Fitness of the population vs Genetic Algorithm Generation')
# Set x-axis ticks to show only full integers
plt.xticks(np.arange(1, NUM_GENERATIONS+1, step=4))
# Set y-axis limits from 0 to 100
plt.ylim(20, 100)

# Add a horizontal line for maximum fitness
plt.axhline(y=max_fitness, color='green', linestyle='dotted', label='Best individual Fitness')

plt.axhline(y=max_average, color='black', linestyle='dotted', label='Best Average Fitness')
# Show maximum fitness value on the y-axis
plt.text(0.5, max_fitness + 2, f'Max individual Fitness: {max_fitness:.2f}%', color='green', fontsize=10)
plt.text(0.5, max_average - 3, f'Max average Fitness: {max_average:.2f}%', color='black', fontsize=10)

# Add caption
plt.figtext(0.5, 0.01, 'Plot of the population fitness. Population Size = '
            +str(POPULATION_SIZE)+' Mutation Rate = '+ str(MUTATION_RATE)+
            ' Survival Rate = '+str(SURVIVAL_RATE) + ' Crossover Rate = ' + str(CROSSOVER_RATE)
            , ha='center', fontsize=10, wrap=True)

# Show legend
plt.legend()

# Save the plot
plt.savefig('fitness_plot_'+'_P'+str(POPULATION_SIZE)+'_'+str(NUM_GENERATIONS)+'.png', bbox_inches='tight', dpi=300)
# Show the plot
plt.show()



    
    


        